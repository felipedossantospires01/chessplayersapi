package br.com.api.chessplayers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChessPlayersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChessPlayersApplication.class, args);
	}

}
