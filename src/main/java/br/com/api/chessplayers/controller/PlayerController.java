package br.com.api.chessplayers.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.chessplayers.entity.Player;
import br.com.api.chessplayers.exception.BusinessException;
import br.com.api.chessplayers.service.PlayerService;

@RestController
@RequestMapping("/players")
public class PlayerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerService.class);

    @Autowired
    private PlayerService playerService;

    @GetMapping
    public ResponseEntity<?> findAll() throws BusinessException{
        LOGGER.info("Buscando todos os jogadores");
        return new ResponseEntity(playerService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{status}")
    public ResponseEntity<?> findAllByStatus(@PathVariable String status) throws BusinessException{
        LOGGER.info("Buscando todos os jogadores pelo status: " + status);
        return new ResponseEntity(playerService.findAllByStatus(status), HttpStatus.OK);
    }

    @GetMapping("/{fideTitle}")
    public ResponseEntity<?> findAllByFideTitle(@PathVariable String fideTitle) throws BusinessException{
        LOGGER.info("Buscando jogadores com titulo fide: " + fideTitle);
        return new ResponseEntity(playerService.findAllByFideTitle(fideTitle), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findPlayerById(@PathVariable Integer id) throws BusinessException{
        LOGGER.info("Buscando jogador pelo id: " + id);
        return new ResponseEntity(playerService.findPlayerById(id), HttpStatus.OK);
    }

    @GetMapping("/{name}")
    public ResponseEntity<?> findPlayerByName(@PathVariable String name) throws BusinessException{
        LOGGER.info("Buscando jogador: " + name);
        return new ResponseEntity(playerService.findPlayerByName(name), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody Player player) throws BusinessException{
        LOGGER.info("Salvando jogador: " + player.getName());
        return new ResponseEntity(playerService.save(player), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Player player) throws BusinessException{
        LOGGER.info("Atualizando dados jogador: " + player.getName());
        return new ResponseEntity(playerService.update(player), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) throws BusinessException{
        LOGGER.info("Deletando jogador de id: " + id);
        playerService.delete(id);
    }

}
