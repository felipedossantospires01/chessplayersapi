package br.com.api.chessplayers.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.api.chessplayers.entity.Player;
import br.com.api.chessplayers.exception.BusinessException;
import br.com.api.chessplayers.repository.PlayerRepository;

@Service
public class PlayerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerService.class);

    private PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public List<Player> findAll() throws BusinessException{
        return playerRepository.findAll();
    }

    public List<Player> findAllByStatus(String status) throws BusinessException{
        if(playerRepository.findPlayersByStatus(status) != null)
        throw new BusinessException("Jogadores com status: " + status + "não encontrados", HttpStatus.NOT_FOUND);
        return playerRepository.findPlayersByStatus(status);
    }

    
    public List<Player> findAllByFideTitle(String fideTitle) throws BusinessException{
        if(playerRepository.findPlayersByFideTitle(fideTitle) != null)
        throw new BusinessException("Jogadores com titulo fide: " + fideTitle + "não encontrados", HttpStatus.NOT_FOUND);
        return playerRepository.findPlayersByFideTitle(fideTitle);
    }

    public Optional<Player> findPlayerByName(String name) throws BusinessException{
        if(playerRepository.findByName(name) != null)
        throw new BusinessException("Jogador: " + name + "não encontrado", HttpStatus.NOT_FOUND);

        return playerRepository.findByName(name);
    }

    public Optional<Player> findPlayerById(Integer id) throws BusinessException{
        if(playerRepository.findById(id) != null)
        throw new BusinessException("Jogador com: " + id + "não encontrado", HttpStatus.NOT_FOUND);

        return playerRepository.findById(id);
    }

    public Player save(Player player) throws BusinessException {
        if (playerRepository.findByName(player.getName()) != null)
            throw new BusinessException("Jogador: " + player.getName() + " já existe na base de dados",
                    HttpStatus.BAD_REQUEST);

        return playerRepository.save(player);
    }

    public Player update(Player player) throws BusinessException{
        return playerRepository.save(player);
    }

    public void delete(Integer id) throws BusinessException{
        playerRepository.deleteById(id);
    }
}
