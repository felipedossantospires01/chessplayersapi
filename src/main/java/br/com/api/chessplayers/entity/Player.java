package br.com.api.chessplayers.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Player implements Serializable{
    
    private static final long serialVersionUID = -2075588127696130293L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "players_seq_id")
    @SequenceGenerator(name = "players_seq_id", allocationSize = 1, initialValue = 1, sequenceName = "players_seq_id")
    private Integer id;
    private String name;
    private Map<String, Long> ratingFide;
    private PlayerStatus status;
    private String federation;
    private String fideTitle;
    private Date birthDate;
    private Integer worldRank;
    private String image;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String,Long> getRatingFide() {
        return this.ratingFide;
    }

    public void setRatingFide(Map<String,Long> ratingFide) {
        this.ratingFide = ratingFide;
    }

    public PlayerStatus getStatus() {
        return this.status;
    }

    public void setStatus(PlayerStatus status) {
        this.status = status;
    }

    public String getFederation() {
        return this.federation;
    }

    public void setFederation(String federation) {
        this.federation = federation;
    }

    public String getFideTitle() {
        return this.fideTitle;
    }

    public void setFideTitle(String fideTitle) {
        this.fideTitle = fideTitle;
    }

    public Date getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getWorldRank() {
        return this.worldRank;
    }

    public void setWorldRank(Integer worldRank) {
        this.worldRank = worldRank;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
