package br.com.api.chessplayers.entity;

import java.util.HashMap;
import java.util.Map;

public enum PlayerStatus {
    ATIVO, INATIVO;

    private static final Map<String, PlayerStatus> MY_MAP = new HashMap<String, PlayerStatus>();
	static {
        for (PlayerStatus myEnum : values()) {
            MY_MAP.put(myEnum.name(), myEnum);
        }
    }

	public static PlayerStatus getByName(String name) {
        return MY_MAP.get(name);
    }
}
