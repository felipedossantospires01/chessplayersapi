package br.com.api.chessplayers.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.api.chessplayers.entity.Player;

public interface PlayerRepository extends JpaRepository<Player, Integer> {
    

    Optional<Player> findByName(String name);

    @Query(nativeQuery = true, value =
    "select * " +
    "from players " +
    "where status = :status "
    )
    List<Player> findPlayersByStatus(String status);

    @Query(nativeQuery = true, value =
    "select * " +
    "from players " +
    "where fideTitle = :fideTitle "
    )
    List<Player> findPlayersByFideTitle(String fideTitle);

}
